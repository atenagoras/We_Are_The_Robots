#include<stdio.h>
#include<math.h>

#include<GL/glut.h>

/* Robôs Evoluidos: Neste projeto, protótipos de robôs feitos em OpenGL são evoluidos para desviar de obstáculos e outros robôs
 * Projeto apresentado para a disciplina Sistemas Evoluticos Aplicados à Robótica, ministrada pelo Profº Eduardo Simões no 2º
 * semestre de 2018 aos cursos de Bacharelado em Ciências da Computação e Bacharelado em Engenharia Elétrica
 * Nome: Atenágoras Souiza Silva (athenagoras@gmail.com) com a ajuda de Lucas (Sasha)
 * Status do Projeto: Fase muito inicial, pois estou aprendendo a usar o OpenGL
 * */
// Para compilar no atual estágio: gcc -lm -lGL -lglut main.c -o robo

float t = 0.0;

void resize(int w, int h){
   glViewport(0, 0, w, h);
}

void idle(){
   glutPostRedisplay();
}

void circle(float cx, float cy, float r){
   glBegin(GL_TRIANGLE_FAN);
      glVertex2f(cx, cy);

      float x = cx;
      float y = cy;

      glVertex2f(cx+r,cy);
      int i;
      for(i=1; i<16; i++){
         x = cx + r*cosf((2*3.1415/16.0)*i);
         y = cy + r*sinf((2*3.1415/16.0)*i);
         glVertex2f(x,y);
      }

      glVertex2f(cx+r, cy);
   glEnd();
}

void drawRobot(float cx, float cy, float theta){
   glMatrixMode(GL_MODELVIEW);

   glLoadIdentity();

   glRotatef(theta, 0.0, 0.0, 1.0);
   glTranslatef(cx, cy, 0.0);

   glColor3f(0.0,0.0,1.0);
   int i;
   for(i=-1; i<2; i++){
      circle(0.4*sinf((3.1415/4.0)*i), 0.4*cosf((3.1415/4.0)*i),0.1);
   }

   glColor3f(1.0,0.0,0.0);
   circle(0.0,0.0,0.5);
}

void render(){
   glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

   drawRobot(0.5*sinf(2*3.1415*t), 0.5*sinf(2*3.1415*t), t*10.0);
   t+=1.0/60.0;

   glutSwapBuffers();
}

void init(){
   glutInitWindowSize(500, 500); // 500px x 500px

   // Double buffering, RGB+Alpha, Buffer de profundidade
   glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

   glutCreateWindow("Pizdec");

   glutReshapeFunc(resize);
   glutDisplayFunc(render);
   glutIdleFunc(idle);

   glClearColor(0.0, 0.0, 0.0, 1.0);
   glEnable(GL_DEPTH_TEST);
}

int main(int argc, char *argv[]){

   glutInit(&argc, argv);
   init();

   glutMainLoop();

   return 0;
}
