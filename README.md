# We_Are_The_Robots

Este projeto tem por objetivo simular em OpenGL o comportamento de Robôs que deverão evoluí-lo a fim de evitar obstáculos e batidas contra outros robôs.

Para compilar:
gcc --lm -lGL -lglut main.c -o robo